@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Witamy</div>

                <div class="panel-body">
                    To jest główna strona aplikacji AZPD - "Aplikacja Zarządająca Pracami Dyplomowymi".
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
